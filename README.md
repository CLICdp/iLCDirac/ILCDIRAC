# iLCDirac

iLCDIRAC is the [Dirac](https://github.com/diracgrid/dirac.git) extension for the ILC/CALICE/FCC Virtual Organizations.

## Documentation

Documentation can be found [here](http://lcd-data.web.cern.ch/lcd-data/doc/ilcdiracdoc/).
