iLCDirac for Production Managers
================================

Documentation about the TransformationSystem
--------------------------------------------

There is a guide about what the TransformationSystem does: :doc:`AdministratorGuide/Systems/Transformation/index`

Configuration in the ConfigurationSystem
----------------------------------------

Some options for productions can be steered via the `configuration system <https://voilcdiracwebapp.cern.ch/DIRAC/?view=tabs&theme=Grey&url_state=1|*DIRAC.ConfigurationManager.classes.ConfigurationManager:,>`_

Which log files to store
````````````````````````

Operations->Defaults->LogFiles -> [Experiment] -> Extension

Set the patterns to match for logfiles to store, e.g.: ".log"

Which SE to use for failover
````````````````````````````

Operation->Defaults->Production-> [Experiment] -> FailOverSE

The the StorageElement to use as failover, do not set to the same SE as your main output SE.
The FailoverSE is only temporally used to store files, when the main SE is not available.


Base path for production output
```````````````````````````````

Operation->Defaults->Production-> [Experiment] -> BasePath

The LFN path where files for given experiment are stored.
Please note: If this path is changed there might be changes needed in the code!


File Catalogs to register production data in
````````````````````````````````````````````

Operation->Defaults->Production-> [Experiment] -> Catalogs
Only the Dirac FileCatalog is used

Production Jobs run locally
---------------------------

Production jobs can be run locally, after having generated the job description file (an xml containing the details of the job).

#. With the usual scripts `dirac-fcc-make-productions` or `dirac-clic-make-productions` generate the xml:

  .. code-block:: shell

    dirac-<fcc or clic>-make-productions -f <your configuration file>

  In alternative, obtain the xml of an existing job by downloading its input sandbox.

#. Download the config tarball (for example ``configVersion = key4hep-devel``) indicated in your configuration file and extract it in the same directory of the xml.

#. Set:

  .. code-block:: shell

		export JOBID=123456789

  Generator seeds are usually set to the job ID.
  In order to have the applications actually using that seed, it is also necessary to add to the ``dirac-jobexec`` command also the flag ``-p JOB_ID=123456789``.

#. Run:

  .. code-block:: shell

    dirac-jobexec <name of your xml>

  as is specified in the documentation of :ref:`dirac-jobexec`.

Some warnings and errors may be generated. They only (marginally) affect the final part of the transformation, for which the local run is not relevant anyway.
