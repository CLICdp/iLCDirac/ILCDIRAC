
.. _fccProdMan:

FCC Production Manager Guide
=============================

.. contents:: Table of Contents

Creating Transformations
------------------------

Transformations are created via the usual production script `dirac-fcc-make-productions`.

The usage of each application module used is described in the `FCCSW tutorials <https://hep-fcc.github.io/fcc-tutorials/master/index.html>`_, 
with more detailed iLCDirac-focused examples at the `HEP-FCC repository <https://github.com/HEP-FCC/FCCDIRAC>`_.

`dirac-fcc-make-productions` generates the transformations starting from configuration files like the one below.
All the configuration files share the same structure. They are divided in sections: some application specific sections, containing parameters affecting only a specific application,
and a general section containing parameters that affect the transformation as a whole.

  .. code-block:: ini
   
    [<first application name>]
    <some parameter> = <some value>
    ...
    <some parameter> = <some value>

    [<second application name>]
    <some parameter> = <some value>
    ...
    <some parameter> = <some value>

    [Production Parameters]
    machine = <machine, which means initial state (always ee)>
    prodGroup = several
    softwareVersion = <key4hep version (or LCG for example)>
    generatorApplication = <generator application name>
    generatorSteeringFile = <generator steering files (a list)>
    configVersion = key4hep-devel-2
    configPackage = fccConfig
    eventsPerJobs = <number of events inside each job (a list)>
    numberOfTasks = <number of tasks composing each job (a list)>
    campaign = <name of the campaign (season+year)>
    energies = <energies of the processes (a list)>
    processes = <name of the processes happening (a list)>
    detectorModel = <name of the detector>
    productionLogLevel = VERBOSE
    outputSE = <name of the storage element where we want to save the data (usually CERN-DST-EOS)>
    finalOutputSE = <name of the magnetic tape location where to copy the data (usually CERN-SRM)>
    MoveStatus = <if we want to copy the data on magnetic tape>
    MoveGroupSize = 10
    ProdTypes = <type of production (at the moment always just Gen)>

  The block ``campaign``, ``energies``, ``processes``, ``detectorModel``, together with ``machine`` determines the path for saving the outputs of the transformation.
  The affected part of the output path will be ``.../fcc/ee/campaign/energy/process/detectorModel/datatype/DiracProdID``, where:

  - ``datatype`` is determined automatically based on the workflow of the transformation,
  - ``DiracProdID`` are the JOB IDs, assigned to the transformations during the submission on DIRAC,
  - ``detectorModel`` will be included only in the output path of transformations that involve the interaction with a detector (so, not during simple event generations).

  It is important to be rigorous with the names, to avoid saving outputs in an incorrect location.
  An example of complete output path could be: ``/eos/experiment/fcc/prod/fcc/ee/winter2023/240gev/WW_incl/idea/delphes/00012345``.

  With ``softwareVersion`` we fix the software version used during the transformation. Individual versions of the applications can also be individually selected in the application specific sections.

  With ``configPackage`` and ``configVersion``  we retrieve files stored on cvmfs that are used when running some applications that are part of the transformation.

  All the parameters just mentioned, as also happens for the application specific parameters, are saved (either directly in the file paths or in the transformation metadata),
  so that all the data produced is tied to both the physical parameters of the process and to the versions of the software used.

  The seeds regulating random processes inside simulations are obtained from the transformations and jobs IDs. 
  This means that, starting from the transformation metadata, is possible to reproduce all the data generated.


Standalone generators
---------------------

The simplest transformations are those that just use a standalone generator, such as KKMC, Whizard, Babayaga, Bhlumi.
They happen in a single step, giving in output the generated events in the desired format.

Event generation: KKMC
^^^^^^^^^^^^^^^^^^^^^^
We are generating events using KKMC.

  .. literalinclude:: kkmc.conf
    :language: ini
    :linenos:

KKMC in principle only requires one application specific parameter: ``EvtType``. 
Regarding its version, KKMC would take the version contained in the key4hep ``softwareVersion`` used for the transformation.
In case it was necessary we could specify a different key4hep ``softwareVersion`` just for KKMC in the application-specific parameters.

The choice of using KKMC as a generator is done with ``generatorApplication = KKMC``.

Event generation: Whizard2
^^^^^^^^^^^^^^^^^^^^^^^^^^

We are generating events using Whizard.

  .. literalinclude:: whizard2.conf
    :language: ini
    :linenos:

Whizard2 in principle only requires two application specific parameter: ``EvtType`` and ``Version``.
Regarding its version, Whizard would take the version contained in the key4hep ``softwareVersion`` used for the transformation.
However, for Whizard, it is recommended to specify the specific Whizard2 version in the application-specific parameters.

The choice of using Whizard2 as a generator is done with ``generatorApplication = whizard2``.

For running Whizard, a Sindarin steering file is also required. We set it with ``generatorSteeringFile = ee_ZHH_1500gev_polp80.sin``.
The Sindarin needs to be contained in the local folder from which the transformation is being created. It will be copied as a string and copied on the machines executing the transformation.

Event generation: Babayaga
^^^^^^^^^^^^^^^^^^^^^^^^^^
We are generating events using Babayaga.

  .. literalinclude:: babayaga.conf
    :language: ini
    :linenos:

Babayaga in principle does not require any application specific parameter. It would take the version contained in the key4hep ``softwareVersion`` used for the transformation.
In case it was necessary we could specify a different key4hep ``softwareVersion`` just for Babayaga in the application-specific parameters.

The choice of using Babayaga as a generator is done with ``generatorApplication = babayaga``.

Event generation: Bhlumi
^^^^^^^^^^^^^^^^^^^^^^^^
We are generating events using Bhlumi.

  .. literalinclude:: bhlumi.conf
    :language: ini
    :linenos:

Bhlumi in principle does not require any application specific parameter. It would take the version contained in the key4hep ``softwareVersion`` used for the transformation.
In case it was necessary we could specify a different key4hep ``softwareVersion`` just for Bhlumi in the application-specific parameters.

The choice of using Babayaga as a generator is done with ``generatorApplication = bhlumi``.

Delphes fast simulation executables
-----------------------------------

Slightly more complicated are the transformations that use Delphes fast simulation executables.

- DelphesSTDHEP_EDM4HEP - for reading STDHEP inputs
- DelphesROOT_EDM4HEP - for reading ROOT files in the Delphes format
- DelphesPythia8_EDM4HEP - for running Pythia8 before the fast simulation
- DelphesPythia8EvtGen_EDM4HEP_k4Interface - for running EvtGen before the fast simulation

k4SimDelphes provides these standalone executables that can read different inputs. They offer the same functionality as the ones available directly from Delphes.
The transformations using Delphes executables may happen in a single step or in two steps, depending on the generator and on the executable.

In particular the executables are run as follows:

  .. code-block:: bash

    ~$ DelphesSTDHEP_EDM4HEP --help
    Usage: DelphesHepMC config_file output_config_file output_file [input_file(s)]
    config_file - configuration file in Tcl format,
    output_config_file - configuration file steering the content of the edm4hep output in Tcl format,
    output_file - output file in ROOT format,
    input_file(s) - input file(s) in STDHEP format,
    with no input_file, or when input_file is -, read standard input.

    ~$ DelphesROOT_EDM4HEP --help
    Usage: DelphesROOT config_file output_config_file output_file input_file(s)
    config_file - configuration file in Tcl format,
    output_config_file - configuration file steering the content of the edm4hep output in Tcl format,
    output_file - output file in ROOT format
    input_file(s) - input file(s) in ROOT format

    ~$ DelphesPythia8_EDM4HEP --help
    Usage: DelphesPythia8config_file output_config_file pythia_card output_file
    config_file - configuration file in Tcl format,
    output_config_file - configuration file steering the content of the edm4hep output in Tcl format,
    pythia_card - Pythia8 configuration file,
    output_file - output file in ROOT format.

    ~$ DelphesPythia8EvtGen_EDM4HEP_k4Interface --help
    Usage: DelphesPythia8EvtGenconfig_file output_config_file pythia_card output_file DECAY.DEC evt.pdl user.dec
    config_file - configuration file in Tcl format,
    output_config_file - configuration file steering the content of the edm4hep output in Tcl format,
    pythia_card - Pythia8 configuration file,
    output_file - output file in ROOT format,
    DECAY.DEC - EvtGen full decay file,
    evt.pdl - EvtGen particle list,
    user.dec - EvtGen user decay file.

DelphesPythia8_EDM4HEP: fast simulation after generation with Pythia8
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We are using the k4SimDelphes DelphesPythia8_EDM4HEP executable for, in a single step, generating events with Pythia8 and processing them in a fast simulation.
(Here we do the same thing that happens in `In Gaudi: Delphes fast simulation after generation with Pythia8`_., but inside Gaudi instead of using directly the Dephes standalone executables).

  .. literalinclude:: delphespythia.conf
    :linenos:
    :language: ini

The choice of using Delphes with Pythia8 as a generator is done with ``generatorApplication = delphesapp``.

The Delphes applications gets in input ``ExecutableName, DetectorCard, OutputCard`` and, in case, ``Version``. Plus, one (or more) ``generatorSteeringFile``.

``DetectorCard`` and ``OutputCard`` are retrieved from cvmfs, as specified with ``configPackage`` and ``configVersion``.
The Pythia8 card, ``<card_name>.cmd``, is instead read from the local folder, as happens for the Sindarin steering file in Whizard. If not found locally, it is retrieved from EOS.
To avoid discrepancies between card energy and the energy in the transformation metadata, the two are compared before creating the transformations.

DelphesPythia8_EDM4HEP: fast simulation after reading (Babayaga) LHE output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pythia8, beside generating events, has also LHE reader capabilities. We can use a specific LHE reader Pythia card for running a Delphes fast simulation on lhef files generated with other generators. Here it is done with Babayaga.
It is the same thing as `DelphesSTDHEP_EDM4HEP: fast simulation after reading (Whizard2) STDHEP output`_. but using different executable and data format.

  .. literalinclude:: delpheslhe.conf
    :language: ini
    :linenos:

The choice of using Delphes with Pythia8 as a generator is done with ``generatorApplication = delphesapp``.

The Delphes applications gets in input ``ExecutableName, DetectorCard, OutputCard`` and, in case, ``Version``. Plus, one (or more) ``generatorSteeringFile``.

``DetectorCard`` and ``OutputCard`` are retrieved from cvmfs, as specified with ``configPackage`` and ``configVersion``.
The Pythia8 card, ``<card_name>.cmd``, is instead read from the local folder, as happens for the Sindarin steering file in Whizard. This time we set the Pythia card from the Delphes application specific parameters. Application specific parameters do not accept lists of parameters, but in this workflow there is no reason to use different Pythia cards, since we are using a simple LHE reader card.

This transformation happens in two steps: first event generation, then fast simulation. To abilitate the second step, we have to specify ``processingAfterGen = delphesapp``.

DelphesPythia8EvtGen_EDM4HEP_k4Interface: fast simulation after generation with EvtGen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We are using the k4SimDelphes DelphesPythia8EvtGen_EDM4HEP_k4Interface executable for, in a single step, generating events with EvtGen and processing them in a fast simulation.

  .. literalinclude:: delphesevtgen.conf
    :linenos:
    :language: ini

The choice of using Delphes with Pythia8 as a generator is done with ``generatorApplication = delphesapp``.

The Delphes applications gets in input ``ExecutableName, DetectorCard, OutputCard`` and, in case, ``Version``. Plus, one (or more) ``generatorSteeringFile``. In addition, to use EvtGen, it receives also.

``DetectorCard`` and ``OutputCard`` (for Pythia) and ``EvtGenParticleList`` and ``EvtGenFullDecay`` (for EvtGen) are retrieved from cvmfs, as specified with ``configPackage`` and ``configVersion``.
The Pythia8 card, ``<card_name>.cmd``, is instead read from the local folder, as happens for the Sindarin steering file in Whizard. If not found locally, it is retrieved from EOS. Same thing happens for the EvtGen decay card.
To avoid discrepancies between pyhtia8 card energy and the energy in the transformation metadata, the two are compared before creating the transformations.

DelphesSTDHEP_EDM4HEP: fast simulation after reading (Whizard2) STDHEP output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We use Whizard2 for generating events in stdhep format. With the DelphesSTDHEP_EDM4HEP k4SimDelphes executable we run the fast simulation on the events.
It is the same thing as `DelphesPythia8_EDM4HEP: fast simulation after reading (Babayaga) LHE output`_. but using different executable and data format.

  .. literalinclude:: delphesstdhep.conf
    :language: ini
    :linenos:

The choice of using Delphes with Pythia8 as a generator is done with ``generatorApplication = delphesapp``.

The Delphes applications gets in input ``ExecutableName, DetectorCard, OutputCard`` and, in case, ``Version``. ``DetectorCard`` and ``OutputCard`` are retrieved from cvmfs, as specified with ``configPackage`` and ``configVersion``.

This transformation happens in two steps: first event generation, then fast simulation. To abilitate the second step, we have to specify ``processingAfterGen = delphesapp``.

DelphesROOT_EDM4HEP: fast simulation after reading ROOT output
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
TODO

In Gaudi: Delphes fast simulation after generation with Pythia8
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here we do the same thing that happens in `DelphesPythia8_EDM4HEP: fast simulation after generation with Pythia8`_., but inside Gaudi instead of using directly the Dephes standalone executables.

  .. literalinclude:: gaudi_delphespythia.conf
    :language: ini
    :linenos:

Gaudi is always run with the ``k4run`` executable.

To specify the desired workflow (that can be fast or full simulation), we set ``GaudiWorkFlow = fastsim`` and then we provide the Gaudi steering file name: ``k4simdelphesalg_pythia.py``.
The Gaudi steering file is always copied from cvmfs, therefore there is no need to have it locally when submitting transformations.

To set the inputs of the Delphes executables, it is necessary to use the ``Arguments`` parameter:
``Arguments = --Pythia8.PythiaInterface.pythiacard pythia8card.cmd --k4SimDelphesAlg.DelphesCard card_IDEA.tcl --k4SimDelphesAlg.DelphesOutputSettings edm4hep_IDEA.tcl``
The ``Arguments`` parameter is available for all the applications. It adds its value to the command running gaudi in the auto-generated script.

``DetectorCard`` and ``OutputCard`` are retrieved from cvmfs, as specified with ``configPackage`` and ``configVersion``.
The Pythia8 card, ``<card_name>.cmd``, is instead read from the local folder, as happens for the Sindarin steering file in Whizard. If not found locally, it is retrieved from EOS.

The choice of using Gaudi as a generator is done with ``generatorApplication = gaudiapp``.

Full simulations
----------------

In Gaudi full simulation: Particle Gun, Geant4, reconstruction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
TODO

Full simulation: Whizard, DDSim, Gaudi
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is a full simulation happening in three steps (Gen, Sim, Rec), using Whizard, DDSim (with Geant4), Gaudi.

  .. literalinclude:: whizardddsimgaudi.conf
    :language: ini
    :linenos:

We are generating events using Whizard (as would be done for `Event generation: Whizard2`_). Whizard2 in principle only requires two application specific parameter: ``EvtType`` and ``Version``.

For the simulation step we use DDSim. The choice of using Gaudi as a generator is done with ``simulationApplication = ddsim``. Its only application specific parameter is a steering file, in this case ``fcc_steer.py``.

Gaudi is selected for the reconstruction with ``reconstructionApplication = gaudiapp``, and run with the ``k4run`` executable. Gaudi always requires a steering file, in this case ``SteeringFile = fccRec_lcio_input.py``.