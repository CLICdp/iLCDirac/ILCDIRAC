Acknowledgements and References
===============================

Acknowledgements
;;;;;;;;;;;;;;;;


LCG Resources
-------------
::

    This work benefited from services provided by the ILC Virtual Organisation, supported by the national resource providers of the EGI Federation.


Ref.: https://operations-portal.egi.eu/vo/view/voname/ilc

OSG Resources
-------------
::

    This research was done using resources provided by the Open Science Grid, which is supported by the National Science Foundation and the U.S. Department of Energy's Office of Science.


Ref.: https://osgconnect.net/citations


References
;;;;;;;;;;

iLCDirac
--------

.. code::
    
    @article{ilcDiracLC,
             author        = "Grefe, C and Poss, S and Sailer, A and Tsaregorodtsev, A",
             title         = "{ILCDIRAC, a DIRAC extension for the Linear Collider community}",
             journal       = "J. Phys.: Conf. Ser.",
             collaboration = "on behalf of The CLIC Detector and Physics Study (CLICdp)",
             number        = "CLICdp-Conf-2013-003",
             volume        = "513",
             pages         = "032077. 5",
             month         = "Nov",
             year          = "2013",
             reportNumber  = "CLICdp-Conf-2013-003",
             url           = "https://cds.cern.ch/record/1626585",
    }

Ref.: https://cds.cern.ch/record/1626585

The Pandora Calibration system is described in

.. code::

     @article{diracCalib,
             author  = {Viazlo, O. and Sailer, A.},
             title   = {Efficient Iterative Calibration on the Grid using {iLCDirac}},
             DOI     = "10.1051/epjconf/202024503003",
             journal = {EPJ Web Conf.},
             year    = 2020,
             volume  = 245,
             pages   = 03003,
     }

Ref.: https://cds.cern.ch/record/2753975


FCC production interfaces are in particular due to

.. code::

    @MastersThesis{valentini24:_integ_fccee_ilcdir_monte_carlo,
             author = {Valentini, Lorenzo},
             title  = {Integration of {FCCee} workflows in {iLCDirac} for large scale {Monte Carlo} productions},
             school = {Universit{\`a} deglie Studi di Padova},
             year   = 2024,
             doi    = {20.500.12608/64700},
             url    = {https://cds.cern.ch/record/2905533},
             note   = {CERN-THESIS-2024-098}
    }


Ref.: https://cds.cern.ch/record/2905533

DIRAC
-----

.. code::

    article{Tsaregorodtsev:1176117,
            author        = "Tsaregorodtsev, A and Bargiotti, M and Brook, N and Ramo,
                             A C and Castellani, G and Charpentier, P and Cioffi, C and
                             Closier, J and Díaz, R G and Kuznetsov, G and Li, Y Y and
                             Nandakumar, R and Paterson, S and Santinelli, R and Smith,
                             A C and Miguelez, M S and Jimenez, S G",
            title         = "{DIRAC: a community grid solution}",
            journal       = "J. Phys.: Conf. Ser.",
            volume        = "119",
            pages         = "062048",
            year          = "2008",
            url           = "https://cds.cern.ch/record/1176117",
    }

Ref.: https://cds.cern.ch/record/1176117

