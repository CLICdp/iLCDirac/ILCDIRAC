Registration
============

Obtaining a Grid Certificate and Registration in ILCDirac
---------------------------------------------------------

Obtain a Grid Certificate
`````````````````````````
Get a grid certificate from your preferred certification authority.

See your local grid expert (there is usually at least one GRID user in every lab
nowadays) for the name of the certification authority corresponding to your
institute.

At CERN (for staff and users) that is
`<https://ca.cern.ch/ca/?template=ee2user>`_. Go to `New Grid User Certificate
<https://ca.cern.ch/ca/user/Request.aspx?template=ee2user>`_


Register to the Virtual Organisation
````````````````````````````````````
Then, depending on your VO, either go to

   ILC: `<https://ilc-auth.cern.ch/start-registration>`_

or

   CALICE: `<https://calice-auth.cern.ch/start-registration>`_

or

   FCC: `<https://fcc-auth.cern.ch/start-registration>`_


or all of them to register yourself as a VO member.

.. note ::

  To register an account, after opening the respective link

   #. Log in to either CERN single sign on (SSO) or via your "Home organisation / eduGAIN"
   #. Fill in the registration form and register
   #. Confirm the registration via the email you will receive
   #. Wait until the request is approved by an administrator
   #. Link your X509 certificate to your account

Registration in iLCDirac is automatically done once you are fully registered in
the auth instance.


Use your Certificate to Obtain a DIRAC proxy
--------------------------------------------

Export your Certificate from the Browser
````````````````````````````````````````
Depending on the way the certificate is delivered you either have to export it
from you browser or import it in your browser. In the end you need to have it
both in your browser, to sign up to a Virtual Organization and use the iLCDirac
web interface; and in your file system to submit jobs, download files etc.

If you cannot find the option, please use `your favourite search engine
<https://google.com>`_ to find a guide for your browser, version and operating
system combination.

Export From the Browser
.......................

You need to export your certificate from the browsers to the ``$HOME/.globus``
folder. Depending on which browser you have the option to export the certificate
varies.

In Firefox 52: Edit ► Preferences ► Advanced ► Certificates ► View certificates ► Your Certificates ► Backup


Import to the Browser
.....................

Remember where you downloaded the certificate to and then import it to your
browser, for example:

In Firefox 52: Edit ► Preferences ► Advanced ► Certificates ► View certificates ► Your Certificates ► Import



.. _convCert:

Convert Certificate for DIRAC
`````````````````````````````

Once registered in the VO and in DIRAC, you need to create the =pem= files. For
this, export your certificate from your browser in ``p12`` format. This ``p12``
file will need to be converted.

The DIRAC installation comes with a handy script to convert the ``p12`` in
``pem`` format. To get this script, you need either to see the `Installing DIRAC
<ilcdiracclient>` section up to the ``dirac-proxy-init -x`` or ``source the
bashrc`` file from your existing local DIRAC installation. Then run ::

  dirac-cert-convert.sh cert.p12

where ``cert.p12`` is the file you obtained from the browser export.
