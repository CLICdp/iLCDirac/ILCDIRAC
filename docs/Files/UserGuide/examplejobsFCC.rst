.. _examplejobsFCC:


Complete Example Submission Scripts
===================================

For description of the functions please see the
:mod:`~ILCDIRAC.Interfaces.API.NewInterface.UserJob` class and the
:mod:`~ILCDIRAC.Interfaces.API.NewInterface.Applications` modules and finally at
the :mod:`~ILCDIRAC.Interfaces.API.DiracILC` class. There are also some more
instructions about :ref:`submittingjobs`.

.. Note ::

  Please use the
  :func:`~ILCDIRAC.Interfaces.API.NewInterface.UserJob.UserJob.setOutputData` function
  to store any output data except for log files.

.. contents:: Table of Contents
