"""Module to run job tests with less in-direction."""

from __future__ import print_function

import glob
import os
import tempfile
import shutil
import pwd

import pytest


from Tests.Utilities.GeneralUtils import running_on_docker


TEST_FILE_DIR = 'Tests/Files'


@pytest.fixture(scope="module")
def diracMagicParseCommandLine():
  """Do the magic command line parsing once for the module."""
  print("Doing the magic")
  from DIRAC import gLogger
  gLogger.setLevel("DEBUG")
  from DIRAC.Core.Base.Script import Script
  Script.parseCommandLine()

  uid = os.getuid()
  user_info = pwd.getpwuid(uid)
  homedir = os.path.join(os.sep + 'home', user_info.pw_name)
  cvmfstestsdir = 'cvmfstests'

  if running_on_docker():
    localsitelocalarea = os.path.join(os.getcwd(), cvmfstestsdir)
  else:
    localsitelocalarea = os.path.join(homedir, cvmfstestsdir)
  from DIRAC import gConfig
  gConfig.setOptionValue('/LocalSite/LocalArea', localsitelocalarea)
  gConfig.setOptionValue('/LocalSite/LocalSE', "CERN-DIP-4")
  gConfig.setOptionValue('/Resources/Countries/local/AssignedTo', 'ch')


def assertOutputFileExists(outputFile):
  """Check if outputfile exists in the random LOCAL_... folder."""
  files = glob.glob(os.path.join('Local_*', outputFile))
  assert len(files) == 1, 'outputfile %s not found' % outputFile


@pytest.fixture
def getJob():
  """Define a generic job, it should be always the same."""
  from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
  myjob = UserJob()
  myjob.setName("Testing")
  myjob.setJobGroup("Tests")
  myjob.setCPUTime(30000)
  myjob.dontPromptMe()
  myjob.setLogLevel("VERBOSE")
  myjob.setPlatform("x86_64-slc5-gcc43-opt")
  myjob.setOutputSandbox(["*.log", "*.xml", "*.sh", "*.cmd", "*.tcl"])
  myjob._addParameter(myjob.workflow, 'TestFailover', 'String', True, 'Test failoverRequest')
  myjob._addParameter(myjob.workflow, 'Platform', 'JDL', "x86_64-slc5-gcc43-opt", 'OS Platform')
  # if self.ildConfig:
  #   myjob.setILDConfig(self.ildConfig)
  return myjob


@pytest.fixture
def jobdir():
  """Create a temporary job directory."""
  print("I will run the tests locally.")
  from DIRAC import gConfig
  localarea = gConfig.getValue("/LocalSite/LocalArea", "")
  if not localarea:
    assert False, "You need to have /LocalSite/LocalArea defined in your dirac.cfg"
  if localarea.find("/afs") == 0:
    print("check ${HOME}/.dirac.cfg and ${DIRAC}/etc/dirac.cfg")
    assert False, "Don't set /LocalSite/LocalArea set to /afs/... as you'll get to install there"
  print("To run locally, I will create a temp directory here.")
  curdir = os.getcwd()
  tmpdir = tempfile.mkdtemp("", dir="./")
  os.chdir(tmpdir)
  yield (tmpdir, curdir)
  os.chdir(curdir)
  if 'CI_JOB_ID' not in os.environ:
    shutil.rmtree(tmpdir)


@pytest.mark.integration
def test_gaudiapp(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the gaudiapp application."""
  _tmpdir, curdir = jobdir
  steeringFile = 'gaudiapp_test_steering.py'
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, steeringFile), os.getcwd())

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import GaudiApp

  dIlc = DiracILC()

  job = getJob
  job.setName("GaudiTest")
  outputFile = "output.root"
  job.setOutputData(outputFile, OutputPath="gaudiTest")

  ga = GaudiApp()
  ga.setVersion('key4hep-latest')
  ga.setExecutableName('k4run')
  ga.setSteeringFile(steeringFile)
  ga.setRandomSeedFlag('--SimG4Svc.seedValue')
  ga.setRandomSeed(100)
  ga.setEnergy(10)
  ga.setNumberOfEvents(25)
  ga.setOutputFile(outputFile)
  ga._extension = 'root'
  ga.datatype = 'gaudi'

  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup gaudiapp"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)


@pytest.mark.integration
def test_delphesapp(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the delphesapp application."""
  _tmpdir, curdir = jobdir
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, 'p8_ee_ggqq_ecm91.cmd'), os.getcwd())
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, 'pythia8card.cmd'), os.getcwd())
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, 'card_IDEA.tcl'), os.getcwd())
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, 'edm4hep_IDEA.tcl'), os.getcwd())

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import DelphesApp

  dIlc = DiracILC()

  job = getJob
  job.setName("DelphesTest")

  outputFile = "output.root"
  job.setOutputData(outputFile, OutputPath="delphesTest")

  delphes = DelphesApp()
  delphes.setVersion('key4hep-latest')
  delphes.setExecutableName("DelphesPythia8_EDM4HEP")
  delphes.setDetectorCard('card_IDEA.tcl')
  delphes.setOutputCard('edm4hep_IDEA.tcl')
  delphes.setPythia8Card('p8_ee_ggqq_ecm91.cmd')
  delphes.setEnergy(91.188)
  delphes.setNumberOfEvents(3)
  delphes.setOutputFile(outputFile)

  res = job.append(delphes)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup delphesapp"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)

@pytest.mark.integration
def test_bhlumi(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the bhlumi application."""
  _tmpdir, curdir = jobdir

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import Bhlumi

  dIlc = DiracILC()

  job = getJob
  job.setName("BhlumiTest")

  outputFile = 'events.lhe'
  job.setOutputData(outputFile, OutputPath="bhlumiTest")

  bhlumi = Bhlumi()
  bhlumi.setVersion('key4hep-latest')
  bhlumi.setNumberOfEvents(100)
  bhlumi.setEnergy(100)
  bhlumi.setRandomSeed(-1)
  bhlumi.setOutputFile(outputFile)

  res = job.append(bhlumi)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup bhlumi"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)

@pytest.mark.integration
def test_babayaga(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the babayaga application."""
  _tmpdir, curdir = jobdir

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import Babayaga

  dIlc = DiracILC()

  job = getJob
  job.setName("BabayagaTest")

  outputFile = 'events.lhe'
  job.setOutputData(outputFile, OutputPath="babayagaTest")

  babayaga = Babayaga()
  babayaga.setVersion('key4hep-latest')
  babayaga.setNumberOfEvents(100)
  babayaga.setEnergy(100)
  babayaga.setRandomSeed(-1)
  babayaga.setOutputFile(outputFile)

  res = job.append(babayaga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup babayaga"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)

@pytest.mark.integration
def test_kkmc(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with the KKMC application."""
  _tmpdir, curdir = jobdir

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import KKMC

  dIlc = DiracILC()

  job = getJob
  job.setName("KKMCTest")

  outputFile = 'events.hepmc'
  job.setOutputData(outputFile, OutputPath="kkmcTest")

  kkmc = KKMC()
  kkmc.setVersion('key4hep_nightly')
  kkmc.setEvtType('Tau')
  kkmc.setNumberOfEvents(30)
  kkmc.setEnergy(91.2)
  kkmc.setOutputFile(outputFile)

  res = job.append(kkmc)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup KKMC"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)

@pytest.mark.integration
def test_unicodeoutput(getJob, jobdir, diracMagicParseCommandLine):
  """Run a job with an application that prints out unicode characters."""
  _tmpdir, curdir = jobdir
  steeringFile = 'unicodesandwich.txt'
  shutil.copy(os.path.join(curdir, TEST_FILE_DIR, steeringFile), os.getcwd())

  from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
  from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication

  dIlc = DiracILC()

  job = getJob
  job.setName("UniCodeTest")
  job.setInputSandbox([steeringFile])

  ga = GenericApplication()
  ga.setSetupScript('/dev/null')
  ga.setScript('/usr/bin/cat')
  ga.setArguments(steeringFile)
  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup genericapp"

  ga = GenericApplication()
  ga.setSetupScript('/dev/null')
  ga.setScript('/usr/bin/cat')
  outputFile = "output.txt"
  ga.setArguments(steeringFile + " > " + outputFile)
  res = job.append(ga)
  if not res['OK']:
    print(res['Message'])
    assert False, "Failed to setup genericapp"

  result = job.submit(dIlc, mode='local')
  assert result['OK'], "Failed to run job: " + result.get('Message')
  assertOutputFileExists(outputFile)
