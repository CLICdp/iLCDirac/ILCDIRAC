#!/bin/bash
find src -name Examples -prune -o -name .ropeproject -prune -o -name "*.py" -exec pylint -E --rcfile=${PYLINT_RCFILE:-.gitlab-ci.d/pylint_py3.rc} --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" {} +
