#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source .gitlab-ci.d/set-reportstyle.sh
export PYTEST_ADDOPTS="${PYTEST_ADDOPTS} --no-cov --junit-xml=junit_se.xml"
pytest -n2 --randomly-dont-reorganize $DIR/../Tests/Integration/Test_SEs.py
