#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source .gitlab-ci.d/set-reportstyle.sh
export ILCDIRAC_BASE_FOLDER=$DIR/..
export COVERAGE_PROCESS_START=$DIR/../.coveragerc
export PYTEST_ADDOPTS=$PYTEST_ADDOPTS" -m 'not integration' --junitxml=junit_unit.xml"
pytest
testResult=$?
sed -i "s@installation/ILCDIRAC/@@g" coverage.xml
sed -i "s@installation/ILCDIRAC@@g" coverage.xml
exit ${testResult}
