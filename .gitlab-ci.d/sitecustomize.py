try:
    import coverage
    coverage.process_startup()
except ImportError:
    print("Coverage not started")
